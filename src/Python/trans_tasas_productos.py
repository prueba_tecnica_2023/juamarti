import pandas as pd

df1 = pd.read_excel(r"../Datos/tasas_productos.xlsx")

df2 = pd.melt(df1,id_vars = ["cod_segmento", "segmento", "cod_subsegmento","calificacion_riesgos"], value_vars = df1.columns[4:12], var_name="tasa", value_name="valor")
df2["tasa"] = df2["tasa"].str.replace("tasa_","").str.capitalize()

df2.to_excel(r"../Datos/tasas_productos_trans.xlsx",index = False)